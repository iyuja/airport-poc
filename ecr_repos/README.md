# ECR Repositories

## Fast build

0. `./deploy.sh`

## Fast destroy

0 `./destroy.sh

## Build

0. `cd environments/all/`
1. Create|Update terraform.tfvars to match your account
1. `export AWS_PROFILE=<your_aws_profile>`
2. `terraform init`
3. `terraform apply -var aws_region=<aws_region>`

## Destroy

0. `terraform destroy -var aws_region=<aws_region>`
