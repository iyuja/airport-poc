#!/bin/bash

: ${AWS_PROFILE:=$1}
: ${AWS_REGION:=$2}
export AWS_PROFILE
export AWS_REGION

if [ "x$AWS_PROFILE" == "x" ] || [ "x$AWS_REGION" == "x" ]; then
  echo "Usage: $0 <aws_profile> <aws_region>"
  exit -1
else
  pushd environments/all/
  terraform init
  terraform apply -var aws_region=${AWS_REGION} -auto-approve
  popd
fi
