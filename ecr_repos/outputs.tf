output "airports_registry_url" {
  value = "${aws_ecr_repository.airports_assembly.repository_url}"
}

output "countries_registry_url" {
  value = "${aws_ecr_repository.countries_assembly.repository_url}"
}

output "registry_id" {
  value = "${aws_ecr_repository.airports_assembly.registry_id}"
}
