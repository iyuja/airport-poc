output "airports_repository" {
  value = "${module.ecr.registry_id}.dkr.ecr.${var.aws_region}.amazonaws.com"
}
