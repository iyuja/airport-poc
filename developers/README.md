# Docker Compose Stack

## Requirements

0. Docker
1. docker-compose

## Notes 

`./run.sh` receives 2 environment variables:
* CA_VERSION = countries-assembly version
* AA_VERSION = airports-assembly version. If unset defaults to 1.0.1

## Run

0. `./run.sh` 
1. To access the logs run `docker-compose logs -f`

## Stop

0. `./stop.sh`

## Build (if needed)

0. `CA_VERSION=1.0.1 AA_VERSION=1.1.0 docker-compose build`
