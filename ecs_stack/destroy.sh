#!/bin/bash

: ${AWS_PROFILE=$1}
: ${AWS_REGION=$2}

if [ "x$AWS_PROFILE" == "x" ] || [ "x$AWS_REGION" == "x" ]; then
  echo "Usage: $0 <aws_profile> <aws_region>"
  exit -1
else
  pushd environments/dev/
  terraform destroy -var aws_region=$AWS_REGION -auto-approve
  popd
fi
