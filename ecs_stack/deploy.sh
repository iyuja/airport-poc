#!/bin/bash

: ${AWS_PROFILE=$1}
: ${AWS_REGION=$2}
: ${AA_VERSION=$3}
: ${CA_VERSION=$4}

if [ "x$AWS_PROFILE" == "x" ] || [ "x$AA_VERSION" == "x" ] || [ "x$CA_VERSION" == "x" ] || [ "x$AWS_REGION" == "x" ]; then
  echo "Usage: $0 <aws_profile> <aws_region> <airports-assembly-version> <countries-assembly-version>"
  exit -1
else
  pushd environments/dev/
  terraform init
  terraform apply -var airports_version=${AA_VERSION} -var countries_version=${CA_VERSION} -auto-approve -var aws_region=${AWS_REGION}
  popd
fi
