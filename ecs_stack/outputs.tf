output "alb_url" {
  value = "http://${aws_alb.alb.dns_name}:${var.alb_listen_port}"
}
