# ECS Stack

## Fast Build

0. `./build.sh`

## Fast Destroy

0. `/destroy.sh`

## Manual Build

0. `cd environments/dev/`
1. `export AWS_PROFILE=<your_aws_profile>`
2. `terraform init`
3. `terraform apply -var aws_region=<aws_region>`

## Manual Destroy

0. `terraform destroy -var aws_region=<aws_region>`
