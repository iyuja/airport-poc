[
  {
    "name": "${name}",
    "image": "${image}",
    "memory": ${memory},
    "essential": true,
    "portMappings": [
      {
        "containerPort": ${port} 
      }
    ]
  }
]
