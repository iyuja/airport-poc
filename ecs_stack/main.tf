# ECS Stack for Airport Service
# Maintainer:

##################
# VPC Definition #
##################

data "aws_availability_zones" "azs" {}

module "ecs_cluster_vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  name               = "lt-ecs-cluster-vpc-${var.env}"
  cidr               = "${var.vpc_cidr}"
  azs                = ["${data.aws_availability_zones.azs.names[0]}", "${data.aws_availability_zones.azs.names[1]}"]
  private_subnets    = ["${var.private_subnet_airports_cidr}", "${var.private_subnet_countries_cidr}"]
  public_subnets     = ["${var.public_subnets_cidr}"]
  enable_nat_gateway = true

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Terraform   = "true"
  }
}

resource "aws_security_group" "alb_sg" {
  name        = "lt-alb-sg-${var.env}"
  description = "LT ALB Security Group"
  vpc_id      = "${module.ecs_cluster_vpc.vpc_id}"

  ingress {
    from_port   = "${var.alb_listen_port}"
    to_port     = "${var.alb_listen_port}"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Terraform   = "true"
  }
}

resource "aws_security_group" "airports_sg" {
  name        = "lt-airports-sg-${var.env}"
  description = "LT Airports Security Group"
  vpc_id      = "${module.ecs_cluster_vpc.vpc_id}"

  ingress {
    from_port = "${var.airports_container_port}"
    to_port   = "${var.airports_container_port}"
    protocol  = "tcp"

    cidr_blocks = [
      "${var.private_subnet_airports_cidr}",
      "${var.public_subnets_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Terraform   = "true"
  }
}

resource "aws_security_group" "countries_sg" {
  name        = "lt-countries-sg-${var.env}"
  description = "LT Countries Security Group"
  vpc_id      = "${module.ecs_cluster_vpc.vpc_id}"

  ingress {
    from_port = "${var.countries_container_port}"
    to_port   = "${var.countries_container_port}"
    protocol  = "tcp"

    cidr_blocks = [
      "${var.private_subnet_countries_cidr}",
      "${var.public_subnets_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Terraform   = "true"
  }
}

##################
# ALB Definition #
##################

resource "aws_alb" "alb" {
  name               = "lt-airports-alb-${var.env}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb_sg.id}", "${module.ecs_cluster_vpc.default_security_group_id}"]
  subnets            = ["${module.ecs_cluster_vpc.public_subnets}"]

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Terraform   = "true"
  }
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = 8000
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "method not supported"
      status_code  = "501"
    }
  }
}

resource "aws_alb_target_group" "airports_target" {
  name        = "lt-airports-target-${var.env}"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = "${module.ecs_cluster_vpc.vpc_id}"
  target_type = "ip"

  health_check {
    interval            = 30
    path                = "${var.airports_healthcheck_endpoint}"
    matcher             = 200
    healthy_threshold   = 3
    unhealthy_threshold = 3
  }

  depends_on = ["aws_alb.alb"]

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Terraform   = "true"
  }
}

resource "aws_alb_target_group" "countries_target" {
  name        = "lt-countries-target-${var.env}"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = "${module.ecs_cluster_vpc.vpc_id}"
  target_type = "ip"

  health_check {
    interval            = 30
    path                = "${var.countries_healthcheck_endpoint}"
    matcher             = 200
    healthy_threshold   = 3
    unhealthy_threshold = 3
  }

  depends_on = ["aws_alb.alb"]

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Terraform   = "true"
  }
}

resource "aws_lb_listener_rule" "airports_listener_rule" {
  listener_arn = "${aws_lb_listener.alb_listener.arn}"
  priority     = 98

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.airports_target.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/airports*"]
  }

  depends_on = ["aws_alb_target_group.airports_target"]
}

resource "aws_lb_listener_rule" "countries_listener_rule" {
  listener_arn = "${aws_lb_listener.alb_listener.arn}"
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.countries_target.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/countries"]
  }

  depends_on = ["aws_alb_target_group.countries_target"]
}

##################
# ECS Definition #
##################

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "lt-ecs-cluster-${var.env}"
}

########################
# Services Definitions #
########################

data "aws_ecr_repository" "airports" {
  name = "${var.airports_repository}"
}

data "template_file" "airports_task_definition" {
  template = "${file("../../templates/task.json.tpl")}"

  vars {
    name   = "${var.airports_container_name}"
    image  = "${data.aws_ecr_repository.airports.repository_url}:${var.airports_version}"
    memory = "${var.airports_task_memory}"
    port   = "${var.airports_container_port}"
  }
}

resource "aws_ecs_task_definition" "airports_task" {
  family                = "lt-airports-task-${var.env}"
  container_definitions = "${data.template_file.airports_task_definition.rendered}"
  network_mode          = "awsvpc"
}

data "aws_ecr_repository" "countries" {
  name = "${var.countries_repository}"
}

data "template_file" "countries_task_definition" {
  template = "${file("../../templates/task.json.tpl")}"

  vars {
    name   = "${var.countries_container_name}"
    image  = "${data.aws_ecr_repository.countries.repository_url}:${var.countries_version}"
    memory = "${var.countries_task_memory}"
    port   = "${var.countries_container_port}"
  }
}

resource "aws_ecs_task_definition" "countries_task" {
  family                = "lt-countries-task-${var.env}"
  container_definitions = "${data.template_file.countries_task_definition.rendered}"
  network_mode          = "awsvpc"
}

resource "aws_ecs_service" "airports_service" {
  name                              = "lt-airport-service-${var.env}"
  cluster                           = "${aws_ecs_cluster.ecs_cluster.id}"
  task_definition                   = "${aws_ecs_task_definition.airports_task.arn}"
  desired_count                     = "${var.airports_desired_count}"
  health_check_grace_period_seconds = 90

  network_configuration {
    subnets         = ["${module.ecs_cluster_vpc.private_subnets[0]}"]
    security_groups = ["${aws_security_group.airports_sg.id}"]
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.airports_target.arn}"
    container_name   = "${var.airports_container_name}"
    container_port   = "${var.airports_container_port}"
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "attribute:ecs.avaliablity-zone"
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  depends_on = ["aws_alb_target_group.airports_target"]
}

resource "aws_ecs_service" "countries_service" {
  name                              = "lt-countries-service-${var.env}"
  cluster                           = "${aws_ecs_cluster.ecs_cluster.id}"
  task_definition                   = "${aws_ecs_task_definition.countries_task.arn}"
  desired_count                     = "${var.countries_desired_count}"
  health_check_grace_period_seconds = 90

  network_configuration {
    subnets         = ["${module.ecs_cluster_vpc.private_subnets[1]}"]
    security_groups = ["${aws_security_group.countries_sg.id}"]
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.countries_target.arn}"
    container_name   = "${var.countries_container_name}"
    container_port   = "${var.countries_container_port}"
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "attribute:ecs.avaliablity-zone"
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  depends_on = ["aws_alb_target_group.countries_target"]
}

###############################
# Instance Profile Definition #
###############################

data "template_file" "ecs_role" {
  template = "${file("../../templates/ecs_assume_role.tpl")}"
}

resource "aws_iam_role" "ecs_instance_role" {
  name               = "lt-ecs-instance-role-${var.env}"
  assume_role_policy = "${data.template_file.ecs_role.rendered}"
}

data "template_file" "ecs_policy" {
  template = "${file("../../templates/ecs_instance_policy.tpl")}"

  vars {
    ecs_cluster_arn = "*"
  }
}

resource "aws_iam_policy" "ecs_instance_policy" {
  name   = "lt-ecs-instance-policy-${var.env}"
  policy = "${data.template_file.ecs_policy.rendered}"
}

resource "aws_iam_role_policy_attachment" "ecs_assume_role_attach" {
  role       = "${aws_iam_role.ecs_instance_role.name}"
  policy_arn = "${aws_iam_policy.ecs_instance_policy.arn}"
}

resource "aws_iam_instance_profile" "ecs_instance_profile" {
  name = "lt-ecs-instance-profile-${var.env}"
  role = "${aws_iam_role.ecs_instance_role.name}"
}

data "template_file" "user_data" {
  template = "${file("../../templates/user_data_ecs.tpl")}"

  vars {
    ecs_cluster_name = "${aws_ecs_cluster.ecs_cluster.name}"
  }
}

##################
# ASG Definition #
##################

resource "aws_launch_configuration" "ecs_cluster_lc" {
  name_prefix          = "lt-ecs-node-${var.env}-"
  image_id             = "${var.ami["${var.aws_region}"]}"
  instance_type        = "${var.instance_type}"
  user_data            = "${data.template_file.user_data.rendered}"
  iam_instance_profile = "${aws_iam_instance_profile.ecs_instance_profile.name}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ecs_cluster_asg" {
  name                 = "lt-ecs-cluster-asg-${var.env}"
  launch_configuration = "${aws_launch_configuration.ecs_cluster_lc.name}"
  min_size             = "${var.asg_min_size}"
  max_size             = "${var.asg_max_size}"
  vpc_zone_identifier  = ["${module.ecs_cluster_vpc.private_subnets}"]
  depends_on           = ["module.ecs_cluster_vpc", "aws_ecs_cluster.ecs_cluster"]

  lifecycle {
    create_before_destroy = true
  }

  tags = [
    {
      key                 = "Environment"
      value               = "${var.env}"
      propagate_at_launch = true
    },
    {
      key                 = "Terraform"
      value               = "true"
      propagate_at_launch = true
    },
    {
      key                 = "Project"
      value               = "${var.project}"
      propagate_at_launch = true
    },
  ]
}
