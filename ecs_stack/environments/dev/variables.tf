variable "aws_region" {
  default = "us-east-1"
}

variable "airports_version" {
  default = "1.0.1"
}

variable "airports_desired_count" {
  default = 1
}

variable "countries_version" {
  default = "1.0.1"
}

variable "countries_desired_count" {
  default = 1
}

variable "asg_max_size" {
  default = 0
}

variable "asg_min_size" {
  default = 0
}

variable "instance_type" {
  default = "t3.medium"
}
