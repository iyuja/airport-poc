output "alb_url" {
  value = "${module.lt.alb_url}"
}

output "airports_version" {
  value = "${var.airports_version}"
}

output "countries_version" {
  value = "${var.countries_version}"
}
