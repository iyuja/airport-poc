provider "aws" {
  region = "${var.aws_region}"
}

module "lt" {
  source            = "../../"
  env               = "dev"
  asg_max_size      = "${var.asg_max_size}"
  asg_min_size      = "${var.asg_min_size}"
  countries_version = "${var.countries_version}"
  airports_version  = "${var.airports_version}"
  instance_type     = "${var.instance_type}"
  aws_region        = "${var.aws_region}"
}
