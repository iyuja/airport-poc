aws_region = "us-east-1"
asg_max_size = 4
asg_min_size = 2
countries_version = "1.0.1"
airports_version = "1.0.1"
instance_type = "t2.medium"
