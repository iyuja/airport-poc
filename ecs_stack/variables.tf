variable "project" {
  default = "airports-api"
}

variable "aws_region" {
  default = ""
}

variable "instance_type" {
  default = "t3.medium"
}

variable "ami" {
  type = "map"

  default = {
    us-east-1 = "ami-0a6be20ed8ce1f055",
    us-east-2 = "ami-0a0c6574ce16ce87a",
    us-west-1 = "ami-04c22ba97a0c063c4",
    us-west-2 = "ami-09568291a9d6c804c",
    eu-west-1 = "ami-066826c6a40879d75",
    eu-west-2 = "ami-0cb31bf24b130a0f9",
    eu-west-3 = "ami-0a0948de946510ec0",
    eu-central-1 = "ami-0b9fee3a2d0596ed1"
  }
}

variable "asg_max_size" {
  default = 3
}

variable "asg_min_size" {
  default = 2
}

variable "env" {
  default = "dev"
}

variable "alb_listen_port" {
  default = 8000
}

variable "airports_repository" {
  default = "ar/airports-assembly"
}

variable "airports_version" {
  default = "1.0.1"
}

variable "airports_container_name" {
  default = "airports"
}

variable "airports_container_port" {
  default = 8080
}

variable "airports_desired_count" {
  default = 1
}

variable "airports_healthcheck_endpoint" {
  default = "/health/ready"
}

variable "airports_task_memory" {
  default = 1024
}

variable "countries_repository" {
  default = "ar/countries-assembly"
}

variable "countries_version" {
  default = "1.0.1"
}

variable "countries_container_name" {
  default = "countries"
}

variable "countries_container_port" {
  default = 8080
}

variable "countries_desired_count" {
  default = 1
}

variable "countries_healthcheck_endpoint" {
  default = "/health/ready"
}

variable "countries_task_memory" {
  default = 1024
}

variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "private_subnet_airports_cidr" {
  default = ["10.0.1.0/24"]
}

variable "private_subnet_countries_cidr" {
  default = ["10.0.2.0/24"]
}

variable "public_subnets_cidr" {
  default = ["10.0.101.0/24", "10.0.102.0/24"]
}
