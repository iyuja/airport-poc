#!/bin/bash

AWS_PROFILE=$1
AWS_REGION=$2
export AWS_PROFILE
export AWS_REGION
: ${AWS_REGION:="eu-west-2"}

if [ "x$AWS_PROFILE" == "x" ] || [ "x$AWS_REGION" == "x" ]; then
  echo "Usage: $0 <aws_profile> [aws_region]"
  exit -1
else
  pushd ecs_stack
  ./destroy.sh
  popd

  pushd ecr_repos
  ./destroy.sh
  popd
fi
