#!/bin/bash

: ${AWS_PROFILE:="$1"}
: ${AWS_REGION:="$2"}
: ${REGISTRY:="$3"}
export AWS_PROFILE
export REGISTRY 

if [ "x$AWS_PROFILE" == "x" ] || [ "x$AWS_REGION" == "x" ]|| [ "x$REGISTRY" == "x" ]; then
  echo "Usage: $0 <aws_profile> <aws_region> <registry_url>"
  exit -1
else
  repos="airports countries"
  for r in $repos; do
    pushd $r
    ./build.sh
    ./push.sh
    popd
  done
fi
