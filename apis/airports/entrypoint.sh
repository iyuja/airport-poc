#!/bin/bash

(nohup java -jar /usr/src/myapp/airports-assembly-$IMAGE_VERSION.jar 2>&1) >> /var/log/airports-assembly.log &
echo "sleeping 10 secs - server loading"
sleep 10 
echo "forcing app initialization"
curl -m 3 -I "http://localhost:8080/health/ready"
tail -f /var/log/airports-assembly.log
