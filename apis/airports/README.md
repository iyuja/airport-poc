# Airports Assembly Container

## Build

### Build current version in VERSION file
`[REGISTRY=<ecr_dns>] ./build.sh`

### Build specific version
`[REGISTRY=<ecr_dns>] [VERSION=<version>] [AWS_PROFILE=<aws_profile>] ./push.sh`

### Upload to ECR
* Use VERSION environment to push an specific version
`[REGISTRY=<ecr_dns>] [VERSION=<version>] [AWS_PROFILE=<aws_profile>] ./push.sh`

### Jar files
Currently the .jar files must be versioned as VERSION format, and should live in this folder
