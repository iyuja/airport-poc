#!/bin/bash

: ${PREFIX:="ar"}
: ${VERSION:=`cat VERSION`}
: ${REGISTRY:=""}
: ${IMAGE:="countries-assembly"} 
: ${AWS_PROFILE:=""}
: ${AWS_REGION:=""}

if [ "x$AWS_PROFILE" == "x" ] || [ "x$AWS_REGION" == "x" ] || [ "x$REGISTRY" == "x" ]; then
  echo "Usage: $0 <aws_profile> <registry_url>"
  exit -1
else
  $(aws --profile ${AWS_PROFILE} ecr get-login --no-include-email --region ${AWS_REGION})
  docker tag ${PREFIX}/${IMAGE}:${VERSION} ${REGISTRY}/${PREFIX}/${IMAGE}:${VERSION}
  docker push ${REGISTRY}/$PREFIX/${IMAGE}:${VERSION}
fi
