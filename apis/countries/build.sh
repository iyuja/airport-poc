#!/bin/bash

: ${PREFIX:="ar"}
: ${VERSION:=`cat VERSION`}
: ${REGISTRY:=""}
: ${IMAGE="countries-assembly"} 

if [ "x$REGISTRY" == "x" ]; then
  echo "Usage: $0 <registry_url>"
  exit -1
else
  docker build --build-arg IMAGE_VERSION=${VERSION} -t ${PREFIX}/${IMAGE}:${VERSION} .
  docker tag ${PREFIX}/${IMAGE}:${VERSION} ${REGISTRY}/${PREFIX}/${IMAGE}:${VERSION}
fi
