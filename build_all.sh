#!/bin/bash

AWS_PROFILE=$1
AWS_REGION=$2
AA_VERSION=$(cat apis/airports/VERSION)
CA_VERSION=$(cat apis/countries/VERSION)
: ${AWS_REGION:="eu-west-2"}
export AWS_PROFILE
export AWS_REGION
export AA_VERSION
export CA_VERSION

if [ "x$AWS_PROFILE" == "x" ] || [ "x$AWS_REGION" == "x" ]; then
  echo "Usage: $0 <aws_profile> [aws_region]"
  exit -1

else

  echo "+++++++++++++++++++++++++++++++++++++++++"
  echo
  echo "Deploying:"
  echo "Airport-Assembly version=${AA_VERSION}"
  echo "Countries-Assembly version=${CA_VERSION}"
  echo "In AWS Account: ${AWS_PROFILE}"
  echo "and AWS Region: ${AWS_REGION}"
  echo
  echo "+++++++++++++++++++++++++++++++++++++++++"
  echo
  read -p "Deploy? (y/n)?: " choice

  case "$choice" in
    "y" )

    echo
    echo "#####################"
    echo "BUILDING REPOSITORIES"
    echo "#####################"
    echo

    pushd ecr_repos/
    ./deploy.sh
    REGISTRY=$(cat environments/all/terraform.tfstate | jq  -M  ".modules[].outputs.airports_repository.value" | grep -v null | sed s/\"//g)
    export REGISTRY
    popd

    echo
    echo "###############"
    echo "BUILDING IMAGES"
    echo "###############"
    echo

    pushd apis/
    ./build_push_all.sh
    popd

    echo
    echo "##################"
    echo "BUILDING ECS STACK"
    echo "##################"
    echo

    pushd ecs_stack/
    ./deploy.sh
    popd
      
    ;;
    * )
    echo "Quitting"
    ;;
  esac
fi
