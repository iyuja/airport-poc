# ECS Airports Service API Stack

## Considerations

* The stack is configured to be deployed in `us-east-*`, `us-west-*` or `eu-west-*`.  If you need to use a different region you'll need to update the terraform variables for it: ecs-ready-ami.

## Requirements

0. Docker installed and running to build the images
1. Place `airports-assembly-1.0.1.jar` and `airports-assembly-1.1.0.jar` inside `apis/airports/`
2. Place `countries-assembly-1.0.1.jar` inside `apis/countries/`
3. AWS' account with permissions to create EC2, ECS, ECR, ALB, VPC
4. aws-cli with a profile setup for account in 3.
5. jq installed
6. terraform installed

## Full build

0. Edit `apis/airports/VERSION` to match the version you want to build and deploy
1. `./build_all.sh <aws_profile> [aws_region]` by default it builds in `eu-west-2`
2. ALB URL will be printed at the end of the `build_all.sh` script
2. Once `./build_all.sh` script completes give a couple of mins for the ECS nodes to be provissioned and target alb to mark the container as available.  ~3mins.

## Full destroy

0. `./destroy_all <aws_profile> [aws_region]`

## Build Order - Step by Step

0. Create ECR Repository.  Use instructions from `ecr_repos/README.md`
1. Use the new ECR Repository DNS to build and push the Docker Images in `apis/{airports,countries}`.  Instructions in `apis/README.md`
2. Launch ECS Stack. Use instructions from `ecs_stack/README.md`

## Local Environments - for Developers

0. Developers can run the stack using an NGINX loadbalancer. Use instructions from `developers/README.md` 
